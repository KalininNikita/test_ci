const path = require('path')
require('dotenv').config()

//const nrwlConfig = require("@nrwl/react/plugins/webpack.js");

module.exports = {
    entry: "./src/index.js",
    mode: process.env.MODE,
    output: {
        path: path.resolve(__dirname, 'public'),
        filename: "./webpack-bundle.js"
    },
    devServer: {
        static: ['img', path.join(__dirname, "public")],
        compress: true,
        port: 3000,    
        client: {
            overlay: true
        }    
    },
    module: {
        rules: [
          {
            test: /\.m?js$/,
            exclude: /(node_modules|bower_components|lib|tscrl.js)/,
            use: {
              loader: "babel-loader"
            }
          },
          {
            test: /\.css$/,
            use: [
              "style-loader",
              {
                loader: "css-loader",
                options: {
                  modules: true
                }
              }
            ]
          },
          {
            test: /\.(png|svg|jpg|gif|ico)$/,
            use: ["file-loader"]
          }
        ]
    },
    experiments: {
      topLevelAwait: true
    },
    resolve : {
      fallback: { 
        zlib: require.resolve("browserify-zlib"),
        assert: require.resolve("assert/"),
        stream: require.resolve("stream-browserify"),
        buffer: require.resolve("buffer/"),
        util: require.resolve("util/"),
        crypto: require.resolve("crypto-browserify"),
        "http": require.resolve("stream-http"),
        "https": require.resolve("https-browserify"),
        "url": require.resolve("url/"),
        "path": require.resolve("path-browserify"),
        "os": require.resolve("os-browserify/browser"),
        "fs": false,
        //"fs": require.resolve("fs"),
         }
    }
}
